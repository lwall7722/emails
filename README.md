<h1>Emails</h1>
These are emails designed, produced, and developed by the Creative Team at the University of Phoenix. These designs and files are copyrighted by the <a href="http://www.phoenix.edu/" target="_blank">University of Phoenix</a>. All rights reserved.
<br />
<ul>
  <li>Currently not accepting commits or changes publicly.</li>
</ul>
